# -*- coding:utf-8 -*-

def res(V, C, M, CL, PR, TF, EU, US, FILE):
    import sys
    # reload(sys)
    # sys.setdefaultencoding('utf-8')
    import datetime as dt  # Модуль, предоставляет классы для обработки времени и даты
    import pandas as pd
    import os  # Модуль предоставляет функции для работы с операционной системой
    import pylab as pl
    import matplotlib.pyplot as plt
    import seaborn as sns

    # загружаем суммы транзакций в датафрейм

    data = pd.read_excel(FILE)

    # nt = F

    # переименовываем название столбца. лучше переменовывать по номеру, т.у. название может быть любым.

    data.rename(columns={'TXN_FROM_AMOUNT': 'txn_from_amount'}, inplace=True)

    # очищаем датафрейм от нулевых транзакций и транзакции меньше 0

    data = data[data.txn_from_amount > 0]

    # доля платежной системы в общем количестве платежей

    visa = V
    master = C
    mir = M
    # доля типов карт в общем количестве платежей

    classic = CL
    premium = PR
    credit_mir = 0.15  # доля кредитных карт мирт от числа классических карт мир

    # курс валют

    EUR_rate = EU
    USD_rate = US

    ITO = 0.001  # стоимость процессинга у технолога

    # оборот прогнозе

    turnover_forecast = TF

    # print('{} сумма всех транзакций'.format(sum(data['txn_from_amount'])))

    # print('{:.0f} средний чек'.format(data['txn_from_amount'].mean()))

    # Рассчитаем себестоимость для каждой операции

    # ТАРИФЫ ПЛАТЕЖНЫХ СИСТЕМ

    ## Master_card tariff

    interchange_Master_premium = 0.021  # процент
    interchange_Master_classic = 0.016  # процент

    fix_mc_before_200 = 0.004  # EUR транзакция до 200 руб.

    fix_mc_above_4000 = 0.015  # EUR транзакция свыше 4000 руб.

    fix_mc_other = 0.009  # EUR остальные транзакции

    fix_mc_before_20EUR = 0.01  # EUR

    percent_mc_above_20EUR = 0.0005  # %

    #  Visa tariff

    interchange_Visa_premium = 0.021  # процент
    interchange_Visa_classic = 0.0165  # процент

    fix_visa = 0.015  # USD

    # Mir tariff

    interchange_Mir_premium = 0.02  # процент
    interchange_Mir_classic = 0.008  # процент
    interchange_Mir_credit = 0.013  # процент

    fix_mir_before_50 = 0.1  # рубли
    percent_mir_before_50_classic = 0.0003  # процент

    fix_mir_above_50_classic = 0.3  # рубли

    fix_mir_before_250_classic = 0.1  # рубли
    fix_mir_before_5000_classic = 0.4  # рубли
    fix_mir_above_5000_classic = 0.6  # рубли

    fix_mir_before_250_premium_credit = 0.2  # рубли
    fix_mir_before_5000_premium_credit = 0.5  # рубли
    fix_mir_above_5000_premium_credit = 0.75  # рубли

    percent_mir_all_premium_credit = 0.0003  # %

    # функция считает себестоимость по каждой транзакции для карт Мир Премиум

    def Mir_card_tarif_premium(row):

        # функция считает себестоимость по каждой транзакции для карт Мир Премиум

        if row['txn_from_amount'] < 50:  # если транзакция меньше 50 руб.

            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_premium) + fix_mir_before_50

        if row['txn_from_amount'] >= 50 and row['txn_from_amount'] < 250:
            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_premium) + fix_mir_before_250_premium_credit

        if row['txn_from_amount'] > 4000:

            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_premium) + fix_mir_above_5000_premium_credit

        else:
            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_premium) + fix_mir_before_5000_premium_credit

        # функция считает себестоимость по каждой транзакции для карт Мир Кридитный продукт

    def Mir_card_tarif_credit(row):

        if row['txn_from_amount'] < 50:  # если транзакция меньше 50 руб.

            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_credit) + fix_mir_before_50

        if row['txn_from_amount'] >= 50 and row['txn_from_amount'] < 250:
            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_credit) + fix_mir_before_250_premium_credit

        if row['txn_from_amount'] > 4000:

            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_credit) + fix_mir_above_5000_premium_credit

        else:
            return row['txn_from_amount'] * (
                        percent_mir_all_premium_credit + interchange_Mir_credit) + fix_mir_before_5000_premium_credit

    # функция считает себестоимость по каждой транзакции для карт Мир Классик

    def Mir_card_tarif_classic(row):

        # функция считает себестоимость по каждой транзакции для карт Мир Классик

        if row['txn_from_amount'] < 50:  # если транзакция меньше 50 руб.

            return row['txn_from_amount'] * (
                        percent_mir_before_50_classic + interchange_Mir_classic) + fix_mir_before_50

        if row['txn_from_amount'] >= 50 and row['txn_from_amount'] < 250:
            return row[
                       'txn_from_amount'] * interchange_Mir_classic + fix_mir_above_50_classic + fix_mir_before_250_classic

        if row['txn_from_amount'] > 4000:

            return row[
                       'txn_from_amount'] * interchange_Mir_classic + fix_mir_above_50_classic + fix_mir_above_5000_classic

        else:
            return row[
                       'txn_from_amount'] * interchange_Mir_classic + fix_mir_above_50_classic + fix_mir_before_5000_classic

    def Master_card_tarif_classic(row):

        # функция считает себестоимость по каждой транзакции для карт Мастеркард Классик

        if row['txn_from_amount'] < 200:  # если транзакция меньше 200 руб.

            return row['txn_from_amount'] * interchange_Master_classic + (
                        fix_mc_before_200 + fix_mc_before_20EUR) * EUR_rate

        if row['txn_from_amount'] > 4000:  # если транзакция свыше

            return row['txn_from_amount'] * (
                        interchange_Master_classic + percent_mc_above_20EUR) + fix_mc_above_4000 * EUR_rate

        else:
            if row['txn_from_amount'] / EUR_rate < 20:

                return row['txn_from_amount'] * interchange_Master_classic + (
                            fix_mc_before_20EUR + fix_mc_other) * EUR_rate  # чек до 30 EUR
            else:
                return row['txn_from_amount'] * (
                            interchange_Master_classic + percent_mc_above_20EUR) + fix_mc_other * EUR_rate  # чек свыше 30 EUR

    def Master_card_tarif_premium(row):

        # функция считает себестоимость по каждой транзакции для карт Мастеркард Премиум

        if row['txn_from_amount'] < 200:  # если транзакция меньше 200 руб.

            return row['txn_from_amount'] * interchange_Master_premium + (
                        fix_mc_before_200 + fix_mc_before_20EUR) * EUR_rate

        if row['txn_from_amount'] > 4000:  # если транзакция свыше

            return row['txn_from_amount'] * (
                        interchange_Master_premium + percent_mc_above_20EUR) + fix_mc_above_4000 * EUR_rate

        else:
            if row['txn_from_amount'] / EUR_rate < 20:

                return row['txn_from_amount'] * interchange_Master_premium + (
                            fix_mc_before_20EUR + fix_mc_other) * EUR_rate  # чек до 30 EUR
            else:
                return row['txn_from_amount'] * (
                            interchange_Master_premium + percent_mc_above_20EUR) + fix_mc_other * EUR_rate  # чек свыше 30 EUR

    def Visa_card_tarif_classic(row):

        # функция считает себестоимость по каждой транзакции для карт Виза Классик

        return row['txn_from_amount'] * interchange_Visa_classic + fix_visa * USD_rate

    def Visa_card_tarif_premium(row):

        # функция считает себестоимость по каждой транзакции для карт Виза Классик

        return row['txn_from_amount'] * interchange_Visa_premium + fix_visa * USD_rate

    # Cчитаем себестоимость для каждой транзакции

    data['Master_card_Classic'] = \
        data.apply(Master_card_tarif_classic, axis=1).reset_index(drop=True)  # axis = 1 - применяем для строк

    data['Visa_Classic'] = \
        data.apply(Visa_card_tarif_classic, axis=1).reset_index(drop=True)  # axis = 1 - применяем для строк

    data['Master_card_Premium'] = \
        data.apply(Master_card_tarif_premium, axis=1).reset_index(drop=True)  # axis = 1 - применяем для строк

    data['Visa_Premium'] = \
        data.apply(Visa_card_tarif_premium, axis=1).reset_index(drop=True)  # axis = 1 - применяем для строк

    data['Mir_Classic'] = \
        data.apply(Mir_card_tarif_classic, axis=1).reset_index(drop=True)

    data['Mir_Premium'] = \
        data.apply(Mir_card_tarif_premium, axis=1).reset_index(drop=True)

    data['Mir_Credit'] = \
        data.apply(Mir_card_tarif_credit, axis=1).reset_index(drop=True)

    # расчитываем себестоимость в рублях по каждой транзакции

    data['Cost_avg'] = (data['Master_card_Premium'] * master * premium +
                        data['Master_card_Classic'] * master * classic +
                        data['Visa_Premium'] * visa * premium + data['Visa_Classic'] * visa * classic +
                        data['Mir_Classic'] * mir * classic * (1 - credit_mir) + data['Mir_Premium'] * mir * premium +
                        data['Mir_Credit'] * mir * classic * credit_mir)

    # расчитываем себестоимость в % по каждой транзакции

    data['Cost_avg_%'] = data['Cost_avg'] / data['txn_from_amount']

    # print('Себестоимость Торгового Эквайринга при текущем распределении чеков составляет {:.2%}'.format(Cost_avg))

    # print('Себестоимость Торгового Эквайринга c учетом ИТО при текущем распределении чеков составляет {:.2%}'.format(Cost_avg + ITO))

    # Расcчитаем Revenue QIWI по каждой транзакции

    # создаем список с комиссией с мерчанта в %

    rate_percent = [float("{0:.1f}".format(i)) for i in pl.frange(1.9, 2.5,
                                                                  0.1)]  # аналог range для типа данных float: создаем список от 1 до 3 с шагом в 0.1

    # создаем список с фиксированной комиссией с мерчанта

    rate_fix = [i for i in range(0, 50, 10)]

    # Во сколько раз планируемый оборот больше оборота в загруженном файле(дальше будем домножать вычесленные NR на это значение)

    share_turnover = turnover_forecast / (data['txn_from_amount'].sum())

    # создаем пустой Датафрейм - матрицу дохода QIWI по тарифам процент плюс фикса

    revenue_QIWI_percent_plus_fix = pd.DataFrame(columns=rate_fix, index=rate_percent)

    # функция считает Revenue QIWI при каждом условии тарифа

    Revenue_sum = 0
    for i in range(len(rate_percent)):  # для i в тарифе процент
        for j in range(len(rate_fix)):  # для j в тарифе fix
            for t in range(data.shape[
                               0] + 1):  # для t в чеке. специально вызываем ошибку, чтобы добавить в датафрейм NR новое значение
                try:
                    Revenue_sum += (data.iloc[t, 0] * (rate_percent[i] / 100) * share_turnover) + \
                                   rate_fix[j] * share_turnover

                except:
                    revenue_QIWI_percent_plus_fix.iloc[i, j] = int(Revenue_sum)
                    Revenue_sum = 0

                    # Считаем сумму затрат по всем транзакциям

    Cost_avg_sum = data['Cost_avg'].sum() * share_turnover + ITO * turnover_forecast

    # print('Себестоимость {:.2}'.format(Cost_avg_sum))

    # Расчитаем NR QIWI

    NR_QIWI_percent_plus_fix = revenue_QIWI_percent_plus_fix - int(Cost_avg_sum)

    # Создадим heatmap для вывода в окне tkinter

    sns.set(font_scale=1.2)
    f, ax = plt.subplots(figsize=(12, 6))
    cmap = sns.diverging_palette(10, 130, as_cmap=True)
    g = sns.heatmap(data=NR_QIWI_percent_plus_fix, annot=True, fmt='d', linewidths=.9, ax=ax, cmap=cmap)
    plt.xlabel('Ставка фикса', fontsize=14)
    plt.ylabel('Ставка в процентах', fontsize=14)
    plt.title('NR QIWI за 12 месяцев работы при выбранной ставке в %+фикса', fontsize=16)
    ax.title.set_position([.5, 1.06])  # Смещаем заголовк выше
    ax.yaxis.labelpad = 20  # Смещаем название оси у левее
    ax.xaxis.labelpad = 20  # Смещаем название оси x ниже
    fig = g.get_figure()
    fig.savefig('matrix.png')

    avg_check = data['txn_from_amount'].mean()  # средний чек, который будем отображать в программе

    Cost_avg = data['Cost_avg_%'].mean()  # считаем средний процент себестоимости по всем транзакциям

    return (avg_check, Cost_avg, fig)

    # print(Cost_avg_sum)
    # print(revenue_QIWI_percent_plus_fix)
    # print(NR_QIWI_percent_plus_fix)

# print(res(0, 0, 0.1, 0.7, 0.3, 1000000000, 75, 65, '/Users/e.spiridonova/Python/WORK/Plateg/Check_amount.xlsx'))