# -*- coding: utf-8 -*-
from tkinter import *
import tkinter.ttk as ttk
import sys
from tkinter import filedialog
from PIL import ImageTk, Image

sys.path.append("./")
import matplotlib

matplotlib.use('PS')  # generate postscript output by default
import pandas as pd
from Aquaring_calculator_v3 import res

parent = Tk()
parent.title("Калькулятор тарифов Эквайринга")

parent.geometry('1400x600+300+200')

canvas = Canvas(parent)
scroll_y = Scrollbar(parent, orient="vertical", command=canvas.yview)

root = Frame(canvas)

# group of widgets

listbox_items = ["Остальное", "Авиабилеты", "ЖКУ"]


# функция которая загружает путь к файлу с
def files():
    files = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx *.xls")])
    link.set("{}".format(files))


# функция выводит расчет тарифов

def result():
    ck, cos, g = res((float(PZ.get())),
                     (float(PC.get())),
                     (float(PM.get())),
                     (float(classic.get())),
                     (float(premium.get())),
                     (float(PV.get())),
                     (float(eur.get())),
                     (float(usd.get())),
                     (link.get()))
    check.set("{:.0f}".format(ck))
    cost.set("{:.2%}".format(cos))
    # photo = PhotoImage(file = 'matrix.png')
    # lbl.update()
    # lbl = Label(root, image = photo)
    # lbl.image = photo
    # lbl.grid(row=12, column=0, sticky=W, columnspan =3, pady=10, padx=10)
    # lbl.pack(fill='x', side='left', anchor = 's')
    update_image = Image.open("matrix.png")
    update_photo = ImageTk.PhotoImage(update_image)
    lbl.configure(image=update_photo)
    lbl.image = update_photo
    lbl.grid(row=12, column=0, sticky=W, columnspan=3, pady=10, padx=10)


# Тип ТСП

l_TSP = Label(root, text='Тип Торгово-сервисного предприятия', bg='yellow')
l_TSP.grid(row=0, column=0, sticky=W, pady=10, padx=10)

e_TSP = ttk.Combobox(root, values=listbox_items, height=1, font=('times', 16), width=80)
e_TSP.set("Остальное")  # установим Combobox в значение Остальное изначально
e_TSP.grid(row=0, column=1, columnspan=3)

# Тип транзакций
var = IntVar()
var.set(0)
l_tranzaction = Label(root, text='Тип транзакций', bg='yellow')
l_tranzaction.grid(row=1, column=0, sticky=W, pady=10, padx=10)

e_purchase = Radiobutton(root, text='Покупка', variable=var, value=0)
e_purchase.grid(row=1, column=1, sticky=W)

e_card2card = Radiobutton(root, text='Перевод с карты на карту', variable=var, value=1)
e_card2card.grid(row=1, column=2, sticky=W)

e_payout = Radiobutton(root, text='Выплата на карту', variable=var, value=2)
e_payout.grid(row=1, column=3, sticky=W)

# Планируемый оборот
l_PV = Label(root, text='Планируемы оборот в год', bg='yellow')
l_PV.grid(row=2, column=0, sticky=W, pady=10, padx=10)

PV = StringVar()
e_PV = Entry(root, textvariable=PV, font=('times', 16), width=80)
e_PV.grid(row=2, column=1, columnspan=3)
e_PV.insert(0, '1000000000')

# Доля платежных систем
l_PS = Label(root, text='Доля платежных систем(%)', bg='yellow')
l_PS.grid(row=3, column=0, sticky=W, pady=10, padx=10)

PM = StringVar()
PZ = StringVar()
PC = StringVar()

l_PZ = Label(root, text='VISA:')
l_PZ.grid(row=3, column=1, sticky=W)
e_PZ = Entry(root, textvariable=PZ, width=6, font=('times', 13))
e_PZ.grid(row=3, column=1, sticky=W, pady=10, padx=70)
e_PZ.insert(0, '0.5')

l_PC = Label(root, text='MASTER CARD:')
l_PC.grid(row=3, column=2, sticky=W)  # pady - отступ сверху
e_PC = Entry(root, textvariable=PC, width=6, font=('times', 13))
e_PC.grid(row=3, column=2, sticky=W, pady=10, padx=108)
e_PC.insert(0, '0.4')

l_PM = Label(root, text='МИР:')
l_PM.grid(row=3, column=3, sticky=W)
e_PM = Entry(root, textvariable=PM, width=6, font=('times', 13))
e_PM.grid(row=3, column=3, sticky=W, pady=10, padx=40)
e_PM.insert(0, '0.1')

# Доля карт Классик и Премиум

classic = StringVar()
premium = StringVar()

share_cards = Label(root, text='Доля карт Классик и Премиум(%)', bg='yellow')
share_cards.grid(row=4, column=0, sticky=W, pady=10, padx=10)

l_classic = Label(root, text='Классик:')
l_classic.grid(row=4, column=1, sticky=W)
e_classic = Entry(root, textvariable=classic, width=6, font=('times', 13))
e_classic.grid(row=4, column=1, sticky=W, pady=10, padx=70)
e_classic.insert(0, '0.7')

l_premium = Label(root, text='Премиум:')
l_premium.grid(row=4, column=2, sticky=W)
e_premium = Entry(root, textvariable=premium, width=6, font=('times', 13))
e_premium.grid(row=4, column=2, sticky=W, pady=10, padx=108)
e_premium.insert(0, '0.3')

# Текущий курс USD и EUR
rate = Label(root, text='Текущий курс', bg='yellow')
rate.grid(row=5, column=0, sticky=W, pady=10, padx=10)

usd = StringVar()
eur = StringVar()

l_usd = Label(root, text='USD:')
l_usd.grid(row=5, column=1, sticky=W)
e_usd = Entry(root, textvariable=usd, width=6, font=('times', 13))
e_usd.grid(row=5, column=1, sticky=W, pady=10, padx=70)
e_usd.insert(0, '67')

l_eur = Label(root, text='EUR:')
l_eur.grid(row=5, column=2, sticky=W)
e_eur = Entry(root, textvariable=eur, width=6, font=('times', 13))
e_eur.grid(row=5, column=2, sticky=W, pady=10, padx=108)
e_eur.insert(0, '75')

# Загрузка файла с чеками

file = Label(root, text='Загрузите файл с чеками', bg='yellow')
file.grid(row=6, column=0, sticky=W, pady=10, padx=10)

b_loadBtn = Button(root, text='Загрузить', command=files, bg="black")
b_loadBtn.grid(row=6, column=1, sticky=W, pady=10, padx=10)
# b_loadBtn.bind("<Button-1>", LoadFile)


link = StringVar()
l_link = Label(root, textvariable=link, text='Путь загруженного файла')
l_link.grid(row=6, column=1, sticky=W, columnspan=2)

# Вывод результатов

b_OUT = Button(root, text='Рассчитать тарифы', command=result, fg='red',
               font=("TkDefaultFont", 13, "bold"), width=20, height=2)
b_OUT.grid(row=7, column=0, sticky=W, pady=10, padx=10)

l_OUT = Label(root, text='Ниже представлен расчет тарифов', font=("TkDefaultFont", 16, "bold"), fg="white", bg='blue')
l_OUT.grid(row=8, column=0, columnspan=4, sticky=N, pady=10, padx=10)

# Вывод среднего чека

l_avg_check = Label(root, text='Средний чек:')
l_avg_check.grid(row=9, column=0, sticky=W, pady=10, padx=10)

check = StringVar()
l_check = Label(root, textvariable=check)
l_check.grid(row=9, column=2, sticky=W, pady=10, padx=10)

# Вывод себестоимости

l_costs = Label(root, text='Себестоимость Торгового Эквайринга при текущем распределении чеков составляет:')
l_costs.grid(row=10, column=0, sticky=W, columnspan=2, pady=10, padx=10)

cost = StringVar()
l_cost = Label(root, textvariable=cost)
l_cost.grid(row=10, column=2, sticky=W, pady=10, padx=10)

# Вывод матрицы тарифов

l_matrix = Label(root, text='Доход QIWI при ставке от ТСП % + фикса:')
l_matrix.grid(row=11, column=0, sticky=W, columnspan=2, pady=10, padx=10)

# Предустановленная картинка матрицы с нулевыми значениями
# photo = PhotoImage(file = 'background_matrix.png')
# lbl = Label(image = photo)
# lbl.image = photo
# lbl.grid(row=12, column=0, sticky=W, columnspan =3, pady=10, padx=10)

# размещаем фото
image = Image.open("background_matrix.png")
photo = ImageTk.PhotoImage(image)
lbl = Label(root, image=photo)
lbl.image = photo  # keep a reference!
lbl.grid(row=12, column=0, sticky=W, columnspan=3, pady=10, padx=10)

# make sure everything is displayed before configuring the scrollregion

canvas.create_window(0, 0, anchor='nw', window=root)

canvas.update_idletasks()
canvas.configure(scrollregion=canvas.bbox('all'),
                 yscrollcommand=scroll_y.set)

canvas.pack(fill='both', expand=True, side='left')
scroll_y.pack(fill='y', side='right')
# lbl.pack(side=BOTTOM)


parent.mainloop()